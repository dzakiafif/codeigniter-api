<?php



class User_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}


	public function insert($nomorAkun)
	{
		$data = [
			'nomor' => $nomorAkun
		];

		return $this->db->insert('user',$data);
	}

	public function listData()
	{
		$this->db->select('*');
		$this->db->from('user');

		return $this->db->get()->result_array();
	}
}
<?php 

require APPPATH . '/libraries/REST_Controller.php';

class Create_user extends REST_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('user/user_model','model');
	}


	public function insert_post()
	{
		$nomorAkun = $this->input->post('nomor');

		$create = $this->model->insert($nomorAkun);

		$this->set_response($create, REST_Controller::HTTP_OK);
	}
}
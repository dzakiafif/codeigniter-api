<?php


require APPPATH . '/libraries/REST_Controller.php';

class List_user extends REST_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user/user_model','model');
	}

	public function index_get()
	{
		$data = $this->model->listData();

		if(!$data) {
			$error = [
				'status' => false,
				'messsage' => 'data tidak ditemukan'
			];

			$this->set_response($error,REST_Controller::HTTP_BAD_REQUEST);
		}

		$output = [
			'status' => true,
			'data' => $data
		];

		$this->set_response($output, REST_Controller::HTTP_OK);
	}
}

